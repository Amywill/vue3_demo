// // useSearch
// import {
//   reactive,
//   toRefs
// } from "@vue/composition-api";

// export default function useSearch(names) {
//   const state = reactive({
//     data: names,
//     searchValue: ''
//   })
//   const onSearch = () => {
//     state.data.forEach(name => 
//       name.isFixSearch = name.value.includes(state.searchValue)
//     )
//   }

//   return {
//     ...toRefs(state),
//     onSearch
//   }
// }
