import { createRouter, createWebHashHistory } from 'vue-router';
import Home from '../views/Home.vue'

const routes = [
{
  path: '/',
  name: 'Home',
  component: Home
},
{
  path: '/about',
  name: 'About',
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
},
{
  path: '/test',
  name: 'Test',
  component: () => import('../views/Test.vue')
},
// {
//   path: '/todo',
//   name: 'Todo',
//   component: () => import('../views/Todo.vue')
// },
{
  path: '/todo',
  name: 'Todo',
  component: () => import('../views/Todo/index.vue')
},
{
  path: '/todo1',
  name: 'Todo',
  component: () => import('../views/Todo/TodoIndex.vue')
},
{
  path: '/vuexDemo',
  name: 'vuexDemo',
  component: () => import('../views/VuexDemo/index.vue')
},
{
  path: '/vuexData',
  name: 'vuexData',
  component: () => import('../views/VuexData/index.vue')
},
{
  path: '/counter',
  name: 'Counter',
  component: () => import('../views/Count/Counter.vue')
},
{
  path: '/counter1',
  name: 'Counter1',
  component: () => import('../views/Count/Counter1.vue')
},
{
  path: '/cart',
  name: 'Cart',
  component: () => import('../views/Cart/index.vue')
},
{
  path: '/demo',
  name: 'Demo',
  component: () => import('../views/Demo.vue')
}
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
