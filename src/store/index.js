import Vuex from 'vuex'

export default Vuex.createStore({
  state: {
    // test: {
    //   a: 1
    // }

    count: 1,
    productionList: [
      {id: 1, name: 'iPad', count: 4, money: 1000},
      {id: 2, name: 'Huawei', count: 7, money: 800},
      {id: 3, name: 'Xiaomi', count: 3, money: 400},
    ],
    cartList: []
  },
  mutations: {
    // setTestA(state, value) {
    //   state.test.a = value
    // }

    increment (state, payload) { // 载荷应该是一个对象
      // 变更状态
      state.count += payload.amount
    },
    decrease (state, payload) {
      state.count -= payload.amount
    },

    setCartList (state, item) {
      state.cartList.push({name: item.name, id: item.id})
      console.log(state, item)
    }
  },
  actions: {
  },
  modules: {
  }
});
